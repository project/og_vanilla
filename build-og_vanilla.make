api = 2
core = 7.x

; Include the definition of how to build Drupal core directly, including patches.
includes[] = "drupal-org-core.make"


; Download the OG Vanilla install profile and recursively build all its dependencies.
projects[og_vanilla][type] = "profile"
projects[og_vanilla][download][type] = "git"
projects[og_vanilla][download][url] = "http://git.drupal.org/project/og_vanilla.git"
projects[og_vanilla][download][branch] = "7.x-2.x"